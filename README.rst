Credentials Cache Collection Utilities
--------------------------------------

This module provides Kerberos 5 credential cache collection utilities for
Python 2.6+ and 3.

When a user authenticates to a Kerberos realm (eg. with ``kinit``), the user
has a short-lived credential in a cache (view it with ``klist``).

You can use this cccolutils module to easily determine if the user has any
valid Kerberos credentials, or what the username is for a particular Kerberos
realm.

Usage
-----

Check if the user has any valid Kerberos credentials:

.. code-block:: python

    import cccolutils

    authenticated = cccolutils.has_creds()
    # authenticated is True or False
    if authenticated:
        print('This user has a valid Kerberos ticket in any credential cache.')
    else:
        print('no valid Kerberos ticket in any credential cache.')

Check the username for the EXAMPLE.COM realm:

.. code-block:: python

    import cccolutils

    username = cccolutils.get_user_for_realm('EXAMPLE.COM')
    # username is a string or None
    if username:
        print('The user in the EXAMPLE.COM realm is %s' % username)
    else:
        print('No credential for the EXAMPLE.COM realm')
